FROM openjdk:8

COPY email-service.jar /email-service/email-service.jar

WORKDIR email-service

ENTRYPOINT ["java", "-jar", "email-service.jar"]